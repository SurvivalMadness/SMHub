package me.xxmatthdxx.smhub.listeners;

import main.java.rank.Rank;
import main.java.rank.RankManager;
import me.xxmatthdxx.smhub.SMHub;
import me.xxmatthdxx.smhub.signs.SignManager;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Matt on 05/05/2015.
 */
public class BlockBreak implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (RankManager.getInstance().getRank(e.getPlayer()) != Rank.OWNER && RankManager.getInstance().getRank(e.getPlayer()) != Rank.ADMIN && RankManager.getInstance().getRank(e.getPlayer()) != Rank.DEV) {
            e.setCancelled(true);
        }
        if (e.getBlock().getType() == Material.SIGN_POST || e.getBlock().getType() == Material.WALL_SIGN) {
            Sign sign = (Sign) e.getBlock().getState();

            if (SignManager.getInstance().getAllSigns().contains(sign) && !(e.getPlayer().isOp())) {
                e.setCancelled(true);
            } else {
                e.setCancelled(false);
            }
        }
    }
}
