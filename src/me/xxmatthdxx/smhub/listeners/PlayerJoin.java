package me.xxmatthdxx.smhub.listeners;

import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import main.java.rank.RankManager;
import me.xxmatthdxx.smhub.API.Title;
import me.xxmatthdxx.smhub.signs.SignManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import util.ItemUtil;

/**
 * Created by Matthew on 2015-05-10.
 */
public class PlayerJoin implements Listener {

    Location lobby = new Location(Bukkit.getWorld("Hub"), -29.095, 4.00000, -271.740, (float) -90.6, 0);

    @EventHandler(priority = EventPriority.NORMAL)
    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        if (RankManager.getInstance().getRank(player) == null) {
            new BukkitRunnable() {
                public void run() {
                    RankManager.getInstance().setRank(player, Rank.DEFAULT);
                }
            }.runTaskLater(SMCore.getPlugin(), 5L);
        }
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(e.getPlayer());
        PlayerInventory pi = e.getPlayer().getInventory();
        pi.clear();
        ItemStack serverselector = ItemUtil.createItem(Material.COMPASS, SMCore.color("&a&lGAME SELECTOR"));
        ItemStack hubselector = ItemUtil.createItem(Material.BEDROCK, SMCore.color("&a&lHUB SELECTOR"));
        ItemStack cosmetics = ItemUtil.createItem(Material.CHEST, SMCore.color("&a&lCOSMETICS"));
        pi.setItem(4, serverselector);
        pi.setItem(2, cosmetics);
        pi.setItem(6, hubselector);
        pi.setHeldItemSlot(4);
        Title.sendTitle(player, "&e&lWelcome to Survival Madness, ", " &a" + player.getName(), 20, 20, 20);
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.teleport(lobby);
        player.getInventory().setHeldItemSlot(4);
        player.sendMessage(SMCore.color("&6----------------------------------------"));
        player.sendMessage(SMCore.color("&aStore: &dstore.survivalmadness.net"));
        player.sendMessage(SMCore.color("&aOwners: &4LionMakerStudios &eand &4bmcc1234"));
        player.sendMessage(SMCore.color("&aDevelopers: &4bmcc1234 &eand &cTormatic"));
        player.sendMessage(SMCore.color("&6----------------------------------------"));
        if (splayer.getRank() == Rank.DEFAULT) {
            e.setJoinMessage(null);
        } else {
            e.setJoinMessage(SMCore.color(splayer.getRank().getColor() + splayer.getRankPrefix() + " " + ChatColor.RESET + splayer.getRank().getColor() + splayer.getCurrentName() + " &r&fhas joined the hub!"));
        }
    }
}
