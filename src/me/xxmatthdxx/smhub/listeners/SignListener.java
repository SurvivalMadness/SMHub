package me.xxmatthdxx.smhub.listeners;

import main.java.RedisManager;
import main.java.server.PlayableServer;
import main.java.server.ServerManager;
import me.xxmatthdxx.smhub.signs.SignManager;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

/**
 * Created by Matt on 03/05/2015.
 */
public class SignListener implements Listener {

    @EventHandler
    public void onSignPlace(SignChangeEvent e) {
        System.out.println(ServerManager.getInstance().isServer(ChatColor.stripColor(e.getLine(0)).trim()));
        if (ServerManager.getInstance().isServer(ChatColor.stripColor(e.getLine(0)).trim())) {
            if (!SignManager.getInstance().getAllSigns().contains(e.getBlock().getState())) {
                System.out.println("Is server");

                PlayableServer server = ServerManager.getInstance().getServer(ChatColor.stripColor(e.getLine(0)));
                SignManager.getInstance().getRawLineData().put((Sign) e.getBlock().getState(), ChatColor.stripColor(e.getLine(0)));
                e.setLine(1, ServerManager.getInstance().getMap(server));
                e.setLine(2, ChatColor.GRAY + ChatColor.BOLD.toString() + ServerManager.getInstance().getOnlinePlayers(server) + "/" + ServerManager.getInstance().getMaxPlayers(server));
                e.setLine(3, ChatColor.DARK_PURPLE + ServerManager.getInstance().getState(server).toString());
                e.setLine(0, ChatColor.LIGHT_PURPLE + "Join-" + e.getLine(0));
                System.out.println(e.getLine(0));
                SignManager.getInstance().addSign(e.getBlock().getLocation());
            } else {

            }
        } else {
            e.getBlock().breakNaturally();
            e.getPlayer().sendMessage(ChatColor.RED + "Not a valid server!");
            return;
        }
    }
}
