package me.xxmatthdxx.smhub.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

/**
 * Created by Shiv on 6/15/2015.
 */
public class MobSpwn implements Listener {

    @EventHandler
    public void onMobSpawn(CreatureSpawnEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onMobSpawn(EntitySpawnEvent e) {
        e.setCancelled(true);
    }
}
