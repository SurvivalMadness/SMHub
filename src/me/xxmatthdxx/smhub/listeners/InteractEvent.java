package me.xxmatthdxx.smhub.listeners;

import main.java.SMCore;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import util.ItemUtil;

/**
 * Created by Matt on 29/05/2015.
 */
public class InteractEvent implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem().getItemMeta().getDisplayName().contains("HUB SELECTOR")) {
                Inventory hub_inv = Bukkit.createInventory(null, 9, SMCore.color("&a&lHUB: &f&LHUB SELECTOR"));
                ItemStack hub1 = ItemUtil.createLoreItem(Material.IRON_BLOCK, SMCore.color("&e&lHUB &7[&a&l1&7]"), SMCore.color("&5&lYOU ARE HERE."));
                hub_inv.setItem(4, hub1);
                p.openInventory(hub_inv);
            }
        }

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem().getItemMeta().getDisplayName().contains("GAME SELECTOR")) {
                Inventory game_inv = Bukkit.createInventory(null, 27, SMCore.color("&a&lHUB: &f&lSERVER SELECTOR"));
                ItemStack hunger_games = ItemUtil.createLoreItem(Material.DIAMOND_SWORD, SMCore.color("&E&lHUNGER GAMES &6- &b&LBETA"), SMCore.color("&7You vs. other players to be the last tribute alive."));
                ItemStack ffa = ItemUtil.createLoreItem(Material.IRON_CHESTPLATE, SMCore.color("&e&lFREE FOR ALL &6- &b&LBETA"), SMCore.color("&7Enter an awesome brawl against other players!"), "", SMCore.color("&7This mode is never ending!"));
                ItemStack hide_and_seek = ItemUtil.createLoreItem(Material.CLAY_BALL, SMCore.color("&e&lHIDE AND SEEK &6- &b&LBETA"), SMCore.color("&7Hide from the seekers!"));
                ItemStack survival = ItemUtil.createLoreItem(Material.GRASS, SMCore.color("&e&lVANILLA SURVIVAL &6- &b&lBETA"), SMCore.color("&7Play vanilla survival with other people!"));
                game_inv.setItem(12, hunger_games);
                game_inv.setItem(13, ffa);
                game_inv.setItem(14, hide_and_seek);
                game_inv.setItem(22, survival);
                e.getPlayer().openInventory(game_inv);
            }
        }

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem().getItemMeta().getDisplayName().contains("COSMETICS")) {
                Inventory inv = Bukkit.getServer().createInventory(null, 9, SMCore.color("&a&LHUB: &f&lCOSMETIC MENU"));
                ItemStack hats = ItemUtil.createColoredItem(Material.LEATHER_HELMET, SMCore.color("&6&lHATS &a- &b&LCUB &eRANK EXCLUSIVE!"), Color.AQUA, SMCore.color("&ePurchase &b&lCUB &eon our store!"), SMCore.color("&dstore.survivalmadness.net"));
                ItemStack armor = ItemUtil.createColoredItem(Material.LEATHER_CHESTPLATE, SMCore.color("&6&lARMOR SELECTOR &a- &a&LKING &eRANK EXCLUSIVE!"), Color.GREEN, SMCore.color("&ePurchase &a&lKING &eon our store!"), SMCore.color("&dstore.survivalmadness.net"));
                ItemStack fountain = ItemUtil.createLoreItem(Material.GOLD_NUGGET, SMCore.color("&6&lFOUNTAIN O' GOLD &a- &a&LKING &eRANK EXCLUSIVE!"), SMCore.color("&ePurchase &a&lKING &eon our store!"), SMCore.color("&dstore.survivalmadness.net"));
                inv.setItem(3, hats);
                inv.setItem(4, armor);
                inv.setItem(5, fountain);
                p.openInventory(inv);
            }
        }
    }
}
