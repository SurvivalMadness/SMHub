package me.xxmatthdxx.smhub.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import me.xxmatthdxx.smhub.SMHub;
import me.xxmatthdxx.smhub.signs.SignManager;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.io.ByteArrayOutputStream;

/**
 * Created by Matt on 03/05/2015.
 */
public class SignClick implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN) {
                Sign sign = (Sign) e.getClickedBlock().getState();
                //This is a game sign
                String serverToTP = SignManager.getInstance().getServerFromSign(sign);
                System.out.println(serverToTP);
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(serverToTP);
                e.getPlayer().sendPluginMessage(SMHub.getPlugin(), "BungeeCord", out.toByteArray());

            } else {
                return;
            }
        }
    }
}
