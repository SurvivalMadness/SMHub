package me.xxmatthdxx.smhub;

import me.xxmatthdxx.smhub.listeners.*;
import me.xxmatthdxx.smhub.signs.SignManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Matthew on 2015-04-22.
 */
public class SMHub extends JavaPlugin {

    private static SMHub plugin;

    /**
     * Get input strewm
     * Input stream handle
     * Buffered reader
     * readline
     */

    public void onEnable() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        plugin = this;
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getConfig().options().copyDefaults(true);
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        saveDefaultConfig();
        SignManager.getInstance().loadSigns();

        System.out.println(Bukkit.getServer().getServerName());
        pm.registerEvents(new SignListener(), this);
        pm.registerEvents(new SignClick(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new InteractEvent(), this);
        pm.registerEvents(new InvClick(), this);
        pm.registerEvents(new DamageEvent(), this);
        pm.registerEvents(new FoodLevelChange(), this);
        pm.registerEvents(new BlockBreak(), this);
        pm.registerEvents(new PlayerQuit(), this);
        pm.registerEvents(new BlockPlace(), this);
        pm.registerEvents(new ItemDrop(), this);
        pm.registerEvents(new MobSpwn(), this);

        SignManager.getInstance().start();
    }

    public void onDisable() {
        plugin = null;
    }

    public static SMHub getPlugin() {
        return plugin;
    }
}
