package me.xxmatthdxx.smhub.signs;

import main.java.server.PlayableServer;
import main.java.server.ServerManager;
import me.xxmatthdxx.smhub.SMHub;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Sign;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Matthew on 2015-04-22.
 */
public class SignManager {

    List<Sign> signs = new ArrayList<>();
    HashMap<Sign, String> rawLineData = new HashMap<>();

    private static SignManager instance = new SignManager();

    public static SignManager getInstance() {
        return instance;
    }

    public Sign getSign(String server) {
        for (Sign sign : signs) {
            sign.update();
            if (sign.getLine(0) == null) {
                System.out.println(sign.getLine(0) + "NULL DEBUG");
            } else if (sign.getLine(0).split("-")[1] == null) {
                for (Map.Entry<Sign, String> signs : rawLineData.entrySet()) {
                    if (signs.getValue().equalsIgnoreCase(server)) {
                        return signs.getKey();
                    }
                }
            }
            System.out.println(sign.getLine(0) + " - DEBUG");
            if (ChatColor.stripColor(sign.getLine(0).split("-")[1]).equalsIgnoreCase(server)) {
                System.out.println(sign.getLine(0) + " - DEBUG");
                return sign;
            }
        }
        return null;
    }

    public void loadSigns() {
        for (String s : SMHub.getPlugin().getConfig().getStringList("signs")) {
            String[] split = s.split(",");
            double x = Double.valueOf(split[0]);
            double y = Double.valueOf(split[1]);
            double z = Double.valueOf(split[2]);
            World world = Bukkit.getWorld(split[3]);
            Location loc = new Location(world, x, y, z);
            Sign sign = (Sign) loc.getWorld().getBlockAt(loc).getState();
            signs.add(sign);
        }
    }

    public HashMap<Sign, String> getRawLineData() {
        return rawLineData;
    }

    public String getRawServer(Sign sign) {
        return rawLineData.get(sign);
    }

    public String getServerFromSign(Sign sign) {
        sign.update();
        if (sign.getLine(0) == null) {
            System.out.println(sign.getLine(0) + "NULL DEBUG");
            return null;
        } else if (sign.getLine(0).split("-").length <= 1) {
            if (rawLineData.containsKey(sign)) {
                return rawLineData.get(sign);
            }
        }
        return ChatColor.stripColor(sign.getLine(0).split("-")[1
                ]);
    }

    public void addSign(Location loc) {
        if (!(loc.getWorld().getBlockAt(loc).getState() instanceof Sign)) {
            System.out.println("Not sign!!!!!");
            return;
        }
        signs.add((Sign) loc.getWorld().getBlockAt(loc).getState());

        List<String> signs = SMHub.getPlugin().getConfig().getStringList("signs");
        signs.add(Double.toString(loc.getBlockX()) + "," + Double.toString(loc.getBlockY()) + "," + Double.toString(loc.getBlockZ()) + "," + loc.getWorld().getName());
        SMHub.getPlugin().getConfig().set("signs", signs);
        SMHub.getPlugin().saveConfig();
        SMHub.getPlugin().reloadConfig();
        for (Sign sign : SignManager.getInstance().getAllSigns()) {
            SignManager.getInstance().updateSign(sign);
        }
        loadSigns();
    }

    public List<Sign> getAllSigns() {
        return signs;
    }

    public void updateSign(final Sign sign) {
        final String servername = getServerFromSign(sign);
        PlayableServer server = ServerManager.getInstance().getServer(servername);
        sign.setLine(1, ServerManager.getInstance().getMap(server));
        System.out.println(server.getOnlineCount());
        sign.setLine(2, ChatColor.GRAY + ChatColor.BOLD.toString() + server.getOnlineCount() + "/" + server.getMaxPlayers());
        sign.setLine(3, ChatColor.DARK_PURPLE + server.getState().toString());
        sign.setLine(0, ChatColor.LIGHT_PURPLE + "Join-" + servername);
        sign.update();
        System.out.println(sign.getLine(2));
    }

    public void start() {
        new BukkitRunnable() {
            public void run() {
                for (Sign sign : signs) {
                    updateSign(sign);
                }
            }
        }.runTaskTimer(SMHub.getPlugin(), 0, 10L);
    }
}