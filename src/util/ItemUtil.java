package util;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.Arrays;
import java.util.List;

/**
 * @author Tormatic | Shiv
 */
public class ItemUtil {

    /**
     * @param item
     * @return
     */
    public static boolean isTool(ItemStack item) {
        return Enchantment.DURABILITY.canEnchantItem(item);
    }

    /**
     * @param item
     * @return
     */
    public static String getDisplayName(ItemStack item) {
        if (item.hasItemMeta()) {
            return item.getItemMeta().getDisplayName();
        } else {
            return "Unknown display name";
        }
    }

    /**
     * @param material
     * @param name
     * @return
     */
    public static ItemStack createItem(Material material, String name) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack createLoreItem(Material material, String name, String... desc) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        if (desc != null && desc.length > 0) meta.setLore(Arrays.asList(desc));
        item.setItemMeta(meta);
        return item;
    }

    /**
     * @param material
     * @param name
     * @param string
     * @return
     */

    /**
     * @param material
     * @param name
     * @param color
     * @return
     */
    public static ItemStack createColoredItem(Material material, String name, Color color, String... desc) {
        ItemStack item = new ItemStack(material, 1);
        LeatherArmorMeta itemmeta = (LeatherArmorMeta) item.getItemMeta();
        if (name != null && !name.isEmpty()) {
            itemmeta.setDisplayName(name);
        }
        if (color != null) {
            itemmeta.setColor(color);
        }
        if (desc != null) {
            itemmeta.setLore(Arrays.asList(desc));
        }
        if (desc != null && desc.length > 0) itemmeta.setLore(Arrays.asList(desc));
        item.setItemMeta(itemmeta);
        return item;
    }

    /**
     * @param material
     * @param name
     * @param desc
     * @param color
     * @return
     */
    public static ItemStack createWoolItem(Material material, String name,
                                           String[] desc, byte color) {
        ItemStack item = new ItemStack(material, 1, (byte) color);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(Arrays.asList(desc));
        item.setItemMeta(meta);
        return item;
    }

    /**
     * @param material
     * @param name
     * @param color
     * @return
     */
    public static ItemStack createWoolItem(Material material, String name,
                                           byte color) {
        ItemStack item = new ItemStack(material, 1, (byte) color);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }
}

